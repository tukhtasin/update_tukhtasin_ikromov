-- Update the rental duration and rental rates of the film with the ID 1
UPDATE film
SET rental_duration = 3, rental_rate = 9.99
WHERE film_id = 1;

--Update the personal data of any existing customer in the database with at least 10 rental and 10 payment records
UPDATE customer
SET first_name = 'Alex', last_name = 'Miller', address_id = 1
WHERE customer_id IN (SELECT customer_id FROM rental GROUP BY customer_id HAVING COUNT(*) >= 10 AND COUNT(payment_id) >= 10);

--Change the create_date value of any existing customer in the database
UPDATE customer
SET create_date = CURRENT_DATE
WHERE customer_id IN (SELECT customer_id FROM rental GROUP BY customer_id HAVING COUNT(*) >= 10);

SELECT film_id, rental_duration, rental_rate FROM film WHERE film_id = 1;

SELECT customer_id, first_name, last_name, address_id FROM customer WHERE customer_id IN (SELECT customer_id FROM rental GROUP BY customer_id HAVING COUNT(*) >= 10);

SELECT customer_id, create_date FROM customer WHERE customer_id IN (SELECT customer_id FROM rental GROUP BY customer_id HAVING COUNT(*) >= 10);


